import matplotlib.pyplot as plt
import numpy as np

def p_decomp(j, p):
    aux_j = j
    res = []
    while (aux_j != 0):
        t = aux_j % p
        res.append(t)
        aux_j = (aux_j - t)/p
    return res

def modulo_star(n, Q):
    if (n == 0):
        return 0
    return (n-1) % (Q-1) + 1

# -----------------------------------------------------------------------------
# computes { d, d \le_p j } 
def shadow(j, p):
    decomp = p_decomp(j, p)
    res = [0]
    p_pow = 1
    for i in decomp:
        aux = []
        for x in range(1,i+1):
            aux += [ x*p_pow + r for r in res]
        res += aux
        p_pow *= p
    return res

# -----------------------------------------------------------------------------
# checks whether |k^{(i)}| \le j^{(i)} for every i 
def is_sum_inf_p(k, j, p, r):
    k_aux = [ p_decomp(x, p) for x in k ]
    k_aux = [ L + [0]*(r-len(L)) for L in k_aux]
    j_aux = p_decomp(j, p)
    j_aux = j_aux + [0]*(r-len(j_aux))
    return all( sum([L[i] for L in k_aux]) <= j_aux[i] for i in range(r))


# -----------------------------------------------------------------------------
# computes { k, \sum_{s=1}^eta s*k_s = d } 
def compute_Td(d, eta):
    if eta == 1:
        return [ [d] ]
    return [ L+[x] for x in range(d//eta+1) for L in compute_Td(d-eta*x, eta-1)]


# -----------------------------------------------------------------------------
# checks if compute_Td works
def check_Td(Td, d, eta):
    return all( sum([L[i]*(i+1) for i in range(eta)]) == d for L in Td)


# -----------------------------------------------------------------------------
# computes D_j as in the paper
def compute_Dj_naive(j, eta, p, r):
    return [ d for d in range(eta*j+1) if any(is_sum_inf_p(k, j, p, r) for k in compute_Td(d, eta))]


# -----------------------------------------------------------------------------
# computes the lifted degree_set
def compute_lifted_degree_set_naive(eta, d, p, r):
    S = []
    q = p**r
    for j in range(d+1):
        Dj = compute_Dj_naive(j, eta, p, r)
        S += [ tuple([i,j]) for i in range(d+1) if all(modulo_star(i+x, q) <= d for x in Dj) ]
    return S


# -----------------------------------------------------------------------------
# tests if the degree set is correct
def long_test_degree_set(deg, eta, d, p, r):
    q = p**r
    for a in deg:
        i,j = a
        S = shadow(j, p)
        for k in cartesian_product([S for u in range(eta)]):
            if (is_sum_inf_p(k, j, p, r) and
                modulo_star(i + sum([k[u]*(u+1) for u in range(eta)]), q) > d):
                return False
    return True


# -----------------------------------------------------------------------------
# plots the degree set and save the figure in "plotname"
def plot_degree_set(S, q, plotname):
    tab_shape =  tuple([q,q])
    tab = np.zeros(shape=tab_shape, dtype=bool)
    for d in S:
        tab[d] = True
    plt.figure(figsize=(5, 5))
    plt.imshow(tab, cmap='Greys', interpolation='nearest')
    plt.gca().invert_yaxis()
    if plotname != None:
        plt.savefig(plotname, bbox_inches="tight", dpi=500)
    plt.close()
    return



# p = 2
# eta = 3
# c = 5

# for r in range(c+1, 9):
#     q = p**r
#     d = p**r - p**(r-c) - 1
#     S = compute_lifted_degree_set_naive(eta, d, p, r)
#     print S
#     plot_degree_set(S, q, "plot_q%d_d%d_eta%d.png" % (q, d, eta))

# print long_test_degree_set(S, eta, d, p, r)

def val(c):
    a = (1.0 - sqrt(2.0)/2)/2
    b = (1.0 + sqrt(2.0)/2)/2
    return (1 - 2**(-c))**2 * ((1-a**c)/(1-a) + (1-b**c)/(1-b))/8

# for c in range(2, 10):
#     print c, val(c)
